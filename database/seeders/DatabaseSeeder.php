<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Theme;
use App\Models\SiteSetting;
use App\Models\Social;
use App\Models\AboutUsPage;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Admin::insert([
          'name'=> 'kabita bhatta',
          'email'=> 'kabitabbhatta@gmail.com',
          'password'=> bcrypt('password'),
        ]);

        Admin::insert([
          'name'=> 'Admin user',
          'email'=> 'admin@gmail.com',
          'password'=> bcrypt('password'),
        ]);
        
        Theme::insert([
          'website_name'=>"Tech coderz",
          'website_tagline'=>"Inspire the Next",
            'favicon'=> "",
        ]);


        SiteSetting::insert([
          'email'=>"info@project.com",
          
        ]);

        Social::insert([
          'facebook'=>''
        ]);
   

    AboutUsPage::insert([
     'page_name'=> 'About Us',
     'page_title'=> 'Manages IT Service Across Various Business',
     'page_subtitle'=> '',
     'page_content'=> '',

     


    ]);
  }
}
