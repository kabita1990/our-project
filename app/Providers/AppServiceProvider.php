<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Theme;
use App\Models\SiteSetting;
use App\Models\social;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        View::composer(['front.*'], function($view){
            $view->with('theme',Theme::first());
        });

        View::composer(['front.*'], function($view){
            $view->with('setting',SiteSetting::first());
        });

        View::composer(['front.*'], function($view){
            $view->with('social',social::first());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
