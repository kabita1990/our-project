<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;


class CategoryController extends Controller
{
    //Index Page
    public function index(){
        $categories = Category::orderBy('category_name','ASC')->get();
        return view('admin.cms.category.index', compact('categories'));
    }

    //Category Store
    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'category_name' => 'required|max:255|unique:categories,category_name',
        ];
        $customMessages = [
            'category_name.required' => 'Category Name is required',
            'category_name.unique' => 'Category Name already exist',
        ];
        $this->validate($request, $rules, $customMessages);
        $category = new Category();
        $category->category_name = $data['category_name'];
        $category->slug = Str::slug($data['category_name']);
        $category->save();
        Session::flash('success_message', 'Category has been Added Successfully');
        return redirect()->back();
    }


    //Category Update
     
     public function update(Request $request, $id){
        $data = $request->all();
        $category = Category::findOrFail($id);
        $rules = [
            'category_name' => 'required|max:255|unique:categories,category_name,'.$category->id,
        ];
        $customMessages = [
            'category_name.required' => 'Category Name is required',
            'category_name.unique' => 'Category Name already exists in our database',
        ];
        $this->validate($request, $rules, $customMessages);
        $category->category_name = $data['category_name'];
        $category->slug = Str::slug($data['category_name']);
        $category->save();
        Session::flash('success_message', 'Category has been Updated Successfully');
        return redirect()->back();
    }

    public function delete($id){
        $category = Category::findOrFail($id);
        $category->delete();
        Session::flash('success_message', 'Category has been Deleted Successfully');
        return redirect()->back();
    }

        public function deleteCheckedCategory(Request $request){
            $ids =$request->ids;
            Category::whereIn('id',$ids)->delete();
            Session::flash('success_message', 'Category has been deleted Successfully');
            return redirect()->back();
        }
           
            

    }
    


