<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Testimonial;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class TestimonialController extends Controller
{
    //Index Page
    public function index(){
        $testimonials = Testimonial::latest()->get();
        return view('admin.testimonial.index',compact ('testimonials'));
    }

     //add Page
     public function add(){
        return view('admin.testimonial.add');
    }

    // Store
   public function store(Request $request){
      $data=$request->all();
      $rules = [
        'name' => 'required|max:255',
        'position'=>'required',
        'details'=>'required',
        'image'=>'required',

    ];
    $customMessages = [
        'name.required' => ' name is required',
        'position.required' => ' position is required',
        'details.required' => ' details is required',
        'image.required' => ' image is required',
    ];
    $this->validate($request, $rules, $customMessages);
    $testimonial= new Testimonial();
    $testimonial->name=$data['name'];
    $testimonial->position=$data['position'];
    $testimonial->details=$data['details'];
    $testimonial->image=$data['image'];

     
    $random = Str::random(10);
    if($request->hasFile('image')){
        $image_tmp = $request->file('image');
        if($image_tmp->isValid()){
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = $random .'.'. $extension;
            $image_path = 'public/uploads/testimonial' . $filename;
            Image::make($image_tmp)->save($image_path);
            $testimonial->image = $filename;
        }
    }
    $testimonial->save();
    Session::flash('success_message', 'Testimonial  has been Added Successfully');
    return redirect()->back();


   }

    //add Page
    public function edit($id){
        $testimonial= Testimonial::findOrFail($id);
        return view('admin.testimonial.edit', compact('testimonial'));
    }

    
    // Store
   public function update(Request $request,$id){
    $data=$request->all();
    $rules = [
      'name' => 'required|max:255',
      'position'=>'required',
      'details'=>'required',

  ];
  $customMessages = [
      'name.required' => ' name is required',
      'position.required' => ' position is required',
      'details.required' => ' details is required',
  ];
  $this->validate($request, $rules, $customMessages);
  $testimonial= Testimonial::findOrFail($id);

  $testimonial->name=$data['name'];
  $testimonial->position=$data['position'];
  $testimonial->details=$data['details'];

   
  $random = Str::random(10);
  if($request->hasFile('image')){
      $image_tmp = $request->file('image');
      if($image_tmp->isValid()){
          $extension = $image_tmp->getClientOriginalExtension();
          $filename = $random .'.'. $extension;
          $image_path = 'public/uploads/testimonial' . $filename;
          Image::make($image_tmp)->save($image_path);
          $testimonial->image = $filename;
      }
  }
  $testimonial->save();
  Session::flash('success_message', 'Testimonial  has been Successfully Updated');
  return redirect()->back();
   }

   public function delete($id){
    $testimonial = Testimonial::findOrFail($id);
    $testimonial->delete();
    $image_path = 'public/uploads/testimonial/';
    if(file_exists($image_path.$testimonial->image)){
      unlink($image_path.$testimonial->image);
    }
    $testimonial->delete();
    Session::flash('success_message', 'testimonial has been deleted Successfully');
    return redirect()->route('testimonial.index') ;
  }
}
