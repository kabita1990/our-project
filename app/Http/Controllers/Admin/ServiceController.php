<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Models\Service;

class ServiceController extends Controller
{
  //Index Page
  public function index(){
    $services=Service::all(); 
     return view('admin.service.index');
  }


  // Add Page
  public function add() {
    return view('admin.service.add');

  }
}
