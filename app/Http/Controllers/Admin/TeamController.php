<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Models\Designation;
use  App\Models\Team;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;



class TeamController extends Controller
{
    //Index Page
    public function index(){
        $teams = Team::orderBy('priority', 'ASC')->get();
        return view ('admin.team.index', compact('teams'));
;
    }

    //ADD
    public function add(){
        $designations = Designation::orderBy('title','ASC')->get();
        return view('admin.team.add', compact('designations'));

    }
// Store Team
    public function store(Request $request){
        $data = $request->all();
        $rules = [
            'name' => 'required|max:255',
            'image' => 'required',
            'designation_id' => 'required',
        ];
        $customMessages = [
            'title.required' => 'Team name is required',
            'image.required' => 'Team Image is required',
            'designation_id.required' => 'Please Select Designation',
            'title.max' => 'You are not allowed to enter more than 255 Characters',
        ];
        $this->validate($request, $rules, $customMessages);
        $team = new Team();
        $team->name = ucwords(strtolower($data['name']));
        $team->priority = $data['priority'];
        $team->designation_id = $data['designation_id'];
        $team->facebook = $data['facebook'];
        $team->twitter = $data['twitter'];
        $team->linkedin = $data['linkedin'];
        $team->instagram = $data['instagram'];

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random .'.'. $extension;
                $image_path = 'public/uploads/team/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $team->image = $filename;
            }
        }

        $team->save();
        Session::flash('success_message', 'Team has been Added Successfully');
        return redirect()->back();
    }

    //ADD
    public function edit($id){
        $team = Team::where('id', $id)->first();
        $designations = Designation::orderBy('title','ASC')->get();
        return view('admin.team.edit', compact('designations', 'team'));

    }
// Store Team
public function update(Request $request,$id){
    $data = $request->all();
    $rules = [
        'name' => 'required|max:255',
        'designation_id' => 'required',
    ];
    $customMessages = [
        'title.required' => 'Team name is required',
        'image.required' => 'Team Image is required',
        'designation_id.required' => 'Please Select Designation',
        'title.max' => 'You are not allowed to enter more than 255 Characters',
    ];
    $this->validate($request, $rules, $customMessages);
    $team = Team::where('id', $id)->first();

    $team->name = ucwords(strtolower($data['name']));
    $team->priority = $data['priority'];
    $team->designation_id = $data['designation_id'];
    $team->facebook = $data['facebook'];
    $team->twitter = $data['twitter'];
    $team->linkedin = $data['linkedin'];
    $team->instagram = $data['instagram'];

    $random = Str::random(10);
    if($request->hasFile('image')){
        $image_tmp = $request->file('image');
        if($image_tmp->isValid()){
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = $random .'.'. $extension;
            $image_path = 'public/uploads/team/' . $filename;
            Image::make($image_tmp)->save($image_path);
            $team->image = $filename;
        }
    }

    $team->save();
    Session::flash('success_message', 'Team has been updated Successfully');
    return redirect()->back();
}
public function delete($id){
    $team = Team::findOrFail($id);
    $image_path = 'public/uploads/team/';
    if(file_exists($image_path.$team->image)){
      unlink($image_path.$team->image);
    }
    $team->delete();
    Session::flash('success_message', 'Team has been deleted Successfully');
    return redirect()->route('team.index') ;
  }


}