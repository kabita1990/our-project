<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Social;
use Illuminate\Support\Facades\Session;
class SocialController extends Controller
{
    
      // SocialSettings
      public function social(){
        $social= Social::first();
        return view ('admin.social.social',compact('social'));
    }
     
    public function socialUpdate( Request $request, $id){
        $data= $request->all();
        $social= Social::first();
        $social -> facebook= $data['facebook'];
        $social -> instagram= $data['instagram'];
        $social -> youtube= $data['youtube'];
        $social -> twitter= $data['twitter'];
        $social -> linkedin= $data['linkedin'];
        $social ->save();
        Session::flash('success_message', ' Social Settings has been Updated Successfully');
        return redirect()->back();


    }

}
