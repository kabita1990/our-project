 <!-- Bootstrap bundle JS -->
 <script src="{{asset('public/Adminpanel/assets/js/bootstrap.bundle.min.js')}}"></script>
  <!--plugins-->
  <script src="{{asset('public/Adminpanel/assets/js/jquery.min.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/plugins/simplebar/js/simplebar.min.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/plugins/metismenu/js/metisMenu.min.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/js/pace.min.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/plugins/chartjs/js/Chart.min.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/plugins/chartjs/js/Chart.extension.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/plugins/apexcharts-bundle/js/apexcharts.min.js')}}"></script>
  <!--app-->

  <script src="{{asset('public/Adminpanel/assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/plugins/datatable/js/dataTables.bootstrap5.min.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/js/table-datatable.js')}}"></script>

  <script  src="{{asset('public/Adminpanel/assets/js/jquery.sweet-alert.custom.js')}}"></script>
  <script  src="{{asset('public/Adminpanel/assets/js/sweetalert.min.js')}}"></script>

  <script src="{{asset('public/Adminpanel/assets/js/app.js')}}"></script>
  <script src="{{asset('public/Adminpanel/assets/js/index.js')}}"></script>
  <script>
    new PerfectScrollbar(".best-product")
 </script>
@yield('js')

</body>

</html>