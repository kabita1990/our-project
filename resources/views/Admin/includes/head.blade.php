<head>
  <!-- Required meta tags -->
  <meta name="csrf-token" content="{{csrf_token()}}">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{asset('public/Adminpanel/assets/images/favicon-32x32.png')}}" type="image/png" />
  <!--plugins-->
  <link href="{{asset('public/Adminpanel/assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"/>
  <link href="{{asset('public/Adminpanel/assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet" />
  <link href="{{asset('public/Adminpanel/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" />
  <link href="{{asset('public/Adminpanel/assets/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet" />
  <link href="{{asset('public/Adminpanel/assets/plugins/datatable/css/dataTables.bootstrap5.min.cs')}}s" rel="stylesheet" />
  <!-- Bootstrap CSS -->
  <link href="{{asset('public/Adminpanel/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
  <link href="{{asset('public/Adminpanel/assets/css/bootstrap-extended.css')}}" rel="stylesheet" />
  <link href="{{asset('public/Adminpanel/assets/css/style.css')}}" rel="stylesheet" />
  <link href="{{asset('public/Adminpanel/assets/css/icons.css')}}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  

  <!-- loader-->
	<link href="{{asset('public/Adminpanel/assets/css/pace.min.css')}}" rel="stylesheet" />

  <!-- Sweetalert -->
<link rel="stylesheet" href="{{asset('public/Adminpanel/assets/css/sweetalert.css')}}">

<link rel="stylesheet"
href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.c
ss" integrity="sha512-
vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3Sa
VpMvR3CA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstraptoggle.min.css" rel="stylesheet">

  <!--Theme Styles-->
  <link href="{{asset('public/Adminpanel/assets/css/dark-theme.css')}}" rel="stylesheet" />
  <link href="{{asset('public/Adminpanel/assets/css/light-theme.css')}}" rel="stylesheet" />
  <link href="{{asset('public/Adminpanel/assets/css/semi-dark.css')}}" rel="stylesheet" />
  <link href="{{asset('public/Adminpanel/assets/css/header-colors.css')}}" rel="stylesheet" />

  <title>Admin Dashboard</title>
</head>