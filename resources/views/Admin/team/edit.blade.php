@extends('admin.includes.admin_design')

@section('content')

 <!--start content-->
 <main class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Team Management</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="{{route('adminDashboard')}}"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">Edit Team</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">	
							<a href="{{route('team.index')}}" class="btn btn-primary"> <i class="bi bi-eye"></i> View All Teams</a>	
					</div>
				</div>
				<!--end breadcrumb-->
				@include('admin.includes._message')
                <div class="row">
					<div class="col-xl-12 mx-auto">
					
					  <div class="card">
              <div class="card-body">
                <div class="border p-3 rounded">
                <h6 class="mb-0 text-uppercase"><span class="text-danger">*</span> ARE REQUIRED FIELDS</h6>
                <hr/>
                <form class="row g-3" method="post" action="{{route('team.update',$team->id)}}"enctype="multipart/form-data">
                  @csrf
                  <div class="col-6">
                    <label class="form-label" for="name">Team Name<span class="text-danger">*</span></label>
                    <input type="text" class="form-control"name="name" id="name" value="{{$team->name}}">
                  </div>




                  <div class="col-3">
                    <label class="form-label" for="priority">Select Designation </label>
                    <select name="designation_id" id="designation_id" class="form-control">
                      <option selected disabled> Selection Designation</option>
                      @foreach($designations as $designation)

                      <option value="{{$designation->id}}" {{$designation->id== $team->designation_id ? "selected": ""}}>{{$designation->title}}</option>

                      @endforeach
                    </select>
                  </div>
                 


                  <div class="col-3">
                    <label class="form-label" for="priority">Team priority </label>
                    <input type="number" class="form-control"name="priority" id="priority" value="{{$team->priority}}">
                  </div>
                 


                  <div class="col-6">
                    <label class="form-label" for="facebook">facebook URL</label>
                    <input type="text" class="form-control"name="facebook" id="facebook" value="{{$team->facebook}}">
                  </div>
                 

                  <div class="col-6">
                    <label class="form-label" for="twitter">Twitter URL</label>
                    <input type="text" class="form-control"name="twitter" id="twitter" value="{{$team->twitter}}">
                  </div>
                 
                  <div class="col-6">
                    <label class="form-label" for="instagram">Instagram URL</label>
                    <input type="text" class="form-control"name="instagram" id="instagram" value="{{$team->instagram}}">
                  </div>
                 

                 <div class="col-6">
                    <label class="form-label" for="linkedin">Linkedin URL</label>
                    <input type="text" class="form-control"name="linkedin" id="linkedin" value="{{$team->linkedin}}">
                  </div>
 


                  

                  <div class="col-4">
                    <label class="form-label" for="image">Team Image <span class="text-danger">*</span></label>
                    <input type="file" class="form-control"name="image" id="image" accept="image/*" onchange="readURL(this)">
                  </div>


                  

                 <md-4></md-4> 

                  <div class="col-md-4">
                      <img src="{{asset('public/uploads/team/'. $team->image)}}" alt="" id="one" width="300px">
                  </div>

                  <div class="col-md-8"></div>


                  <div class="col-2">
                    <div class="d-grid">
                      <button type="submit" class="btn btn-success">Update Information</button>
                    </div>
                  </div>
                </form>
              </div>
              </div>
            </div>

            

					</div>
				</div>
				
			</main>
       <!--end page main-->
@endsection

@section('js')



<script>
 function readURL(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e){
                    $("#one").attr('src', e.target.result).width(300);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

</script>

@endsection